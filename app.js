const express = require('express');
const parser = require('body-parser');
const axios = require('axios')
const ip = require('ip');
const config = require('./config.json');
const helpers = require('./helpers/signatureCreation');
const enums = require('./helpers/enums');

const checkout = require('./checkout');
const merchantHosted = require('./merchantHosted');
const seamlessBasic = require('./seamlessBasic');
const seamlessPro = require('./seamlessPro')
const refunds = require('./refunds')
//const payout = require('./payout')


//const notifyUrl = "http://" + ipAdr + ":" + port + "/notify";
//const notifyUrl = "https://webhook.site/e39bbfdd-6b34-421e-8379-1a2dc9a13238"
const notifyUrl = "";
const path = require('path');


const app = express();

app.locals.ipAdr = ip.address();
app.locals.port = 3000;
//const returnUrlCheckOut = "http://" + app.locals.ipAdr + ":" + app.locals.port + "/result";


app.set('views' , path.join(__dirname,'views'));
app.set('view engine','ejs');
app.set('view options', {layout: 'layout.ejs'});

//app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('public'));
app.use(parser.urlencoded({extended: false}));
app.use(parser.json());

app.use('/checkout',checkout);
app.use('/merchantHosted',merchantHosted);
app.use('/seamlessBasic',seamlessBasic);
app.use('/seamlessPro',seamlessPro);
app.use('/refunds',refunds);
//app.use('/payout',payout);

/*
app.get('/index/index' , (req, res, next)=>{
    console.log("index get hit");
    res.render('index',{
        appId: config.appId, 
        secretKey: config.secretKey, 
        postUrl: config.paths[config.enviornment].cashfreePayUrl});
});
*/


app.post('/calculateSecretKey', (req, res, next)=>{
    const {paymentType} = req.body;
    var {formObj} = req.body;
    const secretKey = config.secretKey;

    switch(paymentType){
        case enums.paymentTypeEnum.checkout: {
            const returnUrl = "http://23f2df2549fd.ngrok.io/checkout/result";
            //http://d4d2e998af9c.ngrok.io -> http://localhost:3000
            formObj.returnUrl = returnUrl;
            formObj.notifyUrl = "";
            formObj.appId = config.appId;
            const signature = helpers.signatureRequest1(formObj, secretKey);
            additionalFields = {
                returnUrl,
                notifyUrl,
                signature,
                appId: config.appId,
            };
            return res.status(200).send({
                status:"success",
                additionalFields,
            });
        }
        case enums.paymentTypeEnum.merchantHosted: {
            var { formObj } = req.body;
            formObj.appId = config.appId;
            formObj.returnUrl = "";
            formObj.notifyUrl = notifyUrl;
            formObj.paymentToken = helpers.signatureRequest2(formObj, config.secretKey);
            return res.status(200).send({
                status: "success",
                paymentData: formObj,
            });
        }
        case enums.paymentTypeEnum.refunds: {
            console.log('calling refunds', req.body)
            var { formObj } = req.body;
            formObj.appId = config.appId;
            formObj.returnUrl = "";
            formObj.notifyUrl = notifyUrl;
            //formObj.url = config.paths[config.enviornment].cashfreeUrl + 'api/v1/order/refund';
            formObj.url = 'https://test.cashfree.com/api/v1/order/refund';
            
            formObj.secretKey = helpers.signatureRequest1(formObj, config.secretKey);
            return res.status(200).send({
                status: "success",
                paymentData: formObj,
            });
        }
        case enums.paymentTypeEnum.seamlessbasic: {
            //for now assume mode to be popup
            //TODO: add support for redirect
            var { formObj } = req.body;
            var additionalFields = {}; 
            formObj.appId = config.appId;
            additionalFields.paymentToken = helpers.signatureRequest3(formObj, config.secretKey);
            additionalFields.notifyUrl = notifyUrl;
            additionalFields.appId = config.appId;
            additionalFields.orderCurrency = "INR";
            return res.status(200).send({
                status: "success",
                additionalFields
            });
        }

        default: {
            console.log("incorrect payment option recieved");
            console.log("paymentOption:", paymentType);
            return res.status(200).send({
                status:"error",
                message:"incorrect payment type sent"
            });
        }
    }
});


/*app.post('/result', (req,res,next)=>{
    //will have to make generic/payment type based
    console.log("result hit");
    console.log(req.body);
    const signature = req.body.signature;
    const derivedSignature = helpers.signatureResponse1(req.body, config.secretKey);
    if(derivedSignature === signature){
        console.log("works");
    }
    else{
        console.log("signature gotten: ", signature);
        console.log("signature derived: ", derivedSignature);
    }
    return res.status(200).send({
        body: req.body
    });
});*/

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

app.post('/callRefund', (req, res, next)=>{
    console.log('req', req.body);
    const url = 'https://test.cashfree.com/api/v1/order/refund';
    const formObj = req.body;
    const reqData = {
        appId: formObj.appId,
        secretKey: formObj.secretKey,
        referenceId: formObj.referenceId,
        refundAmount: formObj.refundAmount,
        refundNote: formObj.refundNote
    }
    var reqDataStr = JSON.stringify(reqData);
    console.log('reqData', reqData, typeof(reqDataStr));
    axios
      .post(url, reqDataStr, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
      })
      .then(res => {
        console.log('Response Received from Axios : ', res);
      })
      .catch(error => {
        console.error(error)
      })
});

//below will not be hit as server is not on https://
app.post('/notify', (req, res, next)=>{
    console.log("notify hit");
    console.log(req.body);
    return res.status(200).send({
        status: "success",
    })
});

app.get('/',(req, res, next)=>{
    console.log("landing page hit");
    /*return res.send({
        status: "success",
        message: "work in progress"
    })*/
    return res.status(200).render("index");
});

app.use((err, req , res , next)=>{
    console.log("error caught");
    console.log(err);
    res.status(500).send({
        status:"fail",
        err: err.message,
    });
})

app.listen('3000', ()=>{
    console.log("server running");
    console.log("config is", config);
})

