module.exports = {
    paymentTypeEnum:{
        checkout: "CHECKOUT",
        merchantHosted:"MERCHANTHOSTED",
        orders:"ORDERS",
        seamlessbasic:"SEAMLESSBASIC",
        refunds:"REFUNDS"
    },

    transactionStatusEnum:{
        cancelled: "CANCELLED",
        failed: "FAILED",
        success: "SUCCESS"
    },
    
    paymentOptions: {
        card: "card",
        nb: "nb",
        wallets: "wallet",
        upi: "upi"
    }
}